const argon = require("argon2");
const userModels = require('../models/users')

exports.readUsers = async (req,res)=>{
    const users = await userModels.selectAllUsers()
    return res.status(200).json({
        success:true,
        message:'success get all users',
        result:users
    })
}

exports.readUsersById = async (req,res)=>{
        const user = await userModels.selectUserById(req.params.id)
        if (user) {
            
            return res.status(200).json({
                 success:true,
                 message:'success get  users',
                 result: user
             })
        }else{
            return res.status(404).json({
                success: false,
                message:    'user not found'
            })
        }
}
exports.destroyUsersById = async (req,res)=>{
    const user = await userModels.deleteUserById(req.params.id)
    if (user) {
        
        return res.status(200).json({
             success:true,
             message:'success delete users',
             result: user
         })
    }else{
        return res.status(404).json({
            success: false,
            message:    'user not found'
        })
    }
}

exports.createUser = async (req,res)=>{
    req.body.password = await argon.hash(req.body.password)
    const user = await userModels.insertUser(req.body)
    if (user) {
        
        return res.status(201).json({
             success:true,
             message:'success create user',
             result: user
         })
    }else{
        return res.status(500).json({
            success: false,
            message: 'error'
        })
    }
}
exports.editUser = async (req,res)=>{
    req.body.password = await argon.hash(req.body.password)
    const user = await userModels.updateUser(req.body, req.params.id)
    if (user) {
        
        return res.status(201).json({
             success:true,
             message:'success update user',
             result: user
         })
    }else{
        return res.status(500).json({
            success: false,
            message: 'error'
        })
    }
}