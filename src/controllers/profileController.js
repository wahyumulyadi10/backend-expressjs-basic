const argon = require("argon2");
const userModels = require('../models/users')
const jwt = require('jsonwebtoken')

exports.getProfile = async (req,res)=>{

    const user = await userModels.selectUserById(req.user.id)
    if (user) {
        
        return res.status(200).json({
             success:true,
             message:'success get  users',
             result: user
         })
    }else{
        return res.status(404).json({
            success: false,
            message:    'user not found'
        })
    }
}