const argon = require("argon2");
const userModel = require("../models/users");
const jwt = require('jsonwebtoken')

exports.login = async (req, res) => {
  const user = await userModel.selectUserByEmail(req.body.email);
  if (user) {

    if (await argon.verify(user.password, req.body.password)) {
      const data ={id:user.id}
      const token = jwt.sign(data,'BC4S-IT-2023!@D3XTR0')
      return res.status(200).json({
        succsess: true,
        message: "login success",
        result: {token}
      });
    } else {
      return res.status(401).json({
        succsess: false,
        message: "login fail, worng password",
      });
    }
  } else {
    return res.status(401).json({
      succsess: false,
      message: "login fail, worng email, or account not register",
    });
  }
};

exports.register = async (req, res) => {
  const emailCheck = await userModel.selectUserByEmail(req.body.email);
  if (emailCheck) {
    return res.status(400).json({ 
      succsess: false,
      message: "error, email already exist",
    });
  } 
    req.body.password = await argon.hash(req.body.password)
    const user = await userModel.insertUser(req.body);

    if (user) {
      return res.status(200).json({
        success: true,
        message: "success register user",
      });
    } else {
      return res.status(400).json({
        success: false,
        message: "error",
      });
    
  }
};
