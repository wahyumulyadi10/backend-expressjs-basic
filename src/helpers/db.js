const pg = require('pg')

const db=  new pg.Pool({
    connectionString: 'postgresql://postgres:postgres@localhost:5432/postgres',
})
db.connect().then(
    ()=>{
        console.log('db postgres connected')
    }
).catch(()=>{
    console.log('failed to connected')
})

module.exports = db