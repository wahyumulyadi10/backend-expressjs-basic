const userRouter = require('express').Router()
const userController = require('../controllers/usersController')


userRouter.get('/', userController.readUsers)
userRouter.get('/:id', userController.readUsersById)
userRouter.delete('/:id', userController.destroyUsersById)
userRouter.post('/', userController.createUser)
userRouter.patch('/:id', userController.editUser)

module.exports = userRouter