const routers = require('express').Router()
const log = require('../middlewares/log')
const authMiddleware = require('../middlewares/auth')

routers.use('/users',log,authMiddleware,require('./users'))
routers.use('/profile',log,authMiddleware,require('./profile'))
routers.use('/auth',authMiddleware, require('./auth'))


module.exports = routers