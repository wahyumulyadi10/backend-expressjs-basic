const profileRouter = require('express').Router()
const profileController = require('../controllers/profileController')


profileRouter.get('/', profileController.getProfile)

module.exports = profileRouter