const jwt = require('jsonwebtoken')
const authMiddleware = (req,res,next)=>{
    const bearer = req.headers.authorization
    if (bearer) {
        const token = bearer.slice(7)
        try {
            const user = jwt.verify(token,'BC4S-IT-2023!@D3XTR0') 
            req.user = user
            next()
        } catch (error) {
            return res.status(401).json({
                status:false,
                message:'unauthorize'
            })
        }
    }else{
        return res.status(401).json({
            status:false,
            message:'unauthorize'
        })
    }
}
module.exports = authMiddleware