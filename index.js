const express = require('express')

const app = express()
app.use(express.urlencoded({extended:false}))
app.use('/',require('./src/routes'))

app.get('/', (req,res)=>{
    return res.json({
        message:'Jalan Nih'
    })
})

app.listen(8080,()=>{
    console.log('Running on port 8080')
})